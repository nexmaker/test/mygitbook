# Summary

* [Introduction](README.md)
* [1.Project manage](doc/pm.md)
* [2. CAD design](doc/cad.md)
* [3. 3D printer](doc/3_3dprinter/assignment.md)
* [4. Electric design ](doc/4electric_design/basicknowledge.md)
* [5. Arduino application](https://www.arduino.cc/) 
    * [IOT basic introduce](doc/9IOT/IOT_basic.md)
    * [NodeMCU-Aliyun Cloud](doc/9IOT/NodeMCUESP8266_ALiYun.md)

